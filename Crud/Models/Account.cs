﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crud.Models
{
    public class Account
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public double Balance { get; set; }
        public string Currency { get; set; }
        public bool IsActive { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
