﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crud.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Crud.Controllers
{
    [Produces("application/json")]
    [Route("api/Accounts")]
    public class AccountsController : Controller
    {
        ApplicationContext db;
        public AccountsController(ApplicationContext _db)
        {
            db = _db;
        }
        // GET: api/Accounts
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                if (db != null)
                {
                    var accounts = await db.Accounts.ToListAsync();
                    if (accounts != null)
                    {
                        return Ok(accounts);
                    }

                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // GET: api/Accounts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                if (db != null)
                {
                    var account = db.Accounts.FirstOrDefaultAsync(x => x.Id == id);
                    if (account != null)
                    {
                        return Ok(account);
                    }

                }
                return NotFound();

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // POST: api/Accounts
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Account account)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (db != null)
                    {
                        await db.Accounts.AddAsync(account);
                        await db.SaveChangesAsync();

                        return Ok(account.Id);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                catch (Exception)
                {

                    return BadRequest();
                }

            }
            return BadRequest();
        }

        // PUT: api/Accounts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody]Account account)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (db != null)
                    {
                        var acc = db.Accounts.FirstOrDefault(x => x.Id == id);
                        if (acc != null)
                        {
                            acc.IsActive = account.IsActive;
                            acc.Number = account.Number;
                            acc.Currency = account.Currency;
                            acc.Balance = account.Balance;
                            await db.SaveChangesAsync();
                            return Ok(acc);
                        }
                    }
                    return NotFound();



                }
                catch (Exception)
                {
                    return BadRequest();
                }
            }

            return BadRequest();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            int result = 0;
            try
            {
                if (db != null)
                {
                    var account = await db.Accounts.FirstOrDefaultAsync(x => x.Id == id);

                    if (account != null)
                    {
                        db.Accounts.Remove(account);
                        result = await db.SaveChangesAsync();
                    }
                }
                if (result == 0)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {

                return BadRequest();
            }

        }
    }
}
