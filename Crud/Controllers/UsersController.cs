﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Crud.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Crud.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        ApplicationContext db;
        public UsersController(ApplicationContext _db)
        {
            db = _db;
        }
        // GET: api/Users
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                if (db != null)
                {
                    var users = await db.Users.ToListAsync();
                    if (users != null)
                    {
                        return Ok(users);
                    }

                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                if (db != null)
                {
                    var users = db.Users.FirstOrDefaultAsync(x => x.Id == id);
                    if (users != null)
                    {
                        return Ok(users);
                    }

                }
                return NotFound();

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (db != null)
                    {
                        await db.Users.AddAsync(user);
                        await db.SaveChangesAsync();

                        return Ok(user.Id);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                catch (Exception)
                {

                    return BadRequest();
                }

            }
            return BadRequest();
        }

    }
}
